# Config

This repo contains example config for [Desktop](https://gitlab.com/reactivemarkets/desktop).

## Examples

Single window application

```bash
desktop -f https://gitlab.com/reactivemarkets/Desktop/examples/config/raw/master/examples/single-window.yaml
```

Basic multi-window application

```bash
desktop -f https://gitlab.com/reactivemarkets/Desktop/examples/config/raw/master/examples/multi-window.yaml
```
